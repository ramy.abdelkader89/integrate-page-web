// Gulp 4
var gulp = require('gulp');
var sass = require('gulp-sass');
var uglifycss = require('gulp-uglifycss');

/* File css simple */
gulp.task('scss', function () {
    return gulp.src('./assets/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./assets/css'));
});
/* File minify*/
gulp.task('css', function () {
    gulp.src('./assets/css/style.css')
        .pipe(uglifycss({
            "uglyComments": true
        }))
        .pipe(gulp.dest('./assets/dist/'));
});

/* Mode run watch */
gulp.task('run', gulp.series('scss', 'css'));
gulp.task('watch', function (){
    gulp.watch('./assets/scss/*.scss', gulp.series('scss'));
    gulp.watch('./assets/css/style.css', gulp.series('css'));    
});
gulp.task('default', gulp.parallel('run', 'watch'));