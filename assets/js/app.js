$(document).ready(function(){
    // error cross origin to check
    $.getJSON( "destinations.json", function( data ) {                    
        var items = [];
        $.each( data.destinations, function( key, val ) {
            items.push( '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 mb-4">'+
                '<div class="card">'+
                    '<div id="carouselExampleControls5" class="carousel slide" data-ride="carousel">'+
                        '<div class="carousel-inner">'+
                                '<div class="carousel-item">'+
                                        '<figure>'+
                                            '<img class="d-block w-100" src="assets/images/IMPIANA_RESORT_SAMUI.png" alt="Maurice">'+
                                            "<figcaption>"+val.upto+"</figcaption>"+
                                        '</figure>'+
                                    '</div>'+
                            '<div class="carousel-item active">'+
                                '<figure>'+
                                    '<img class="d-block w-100" src="assets/images/STHALA_MARC_PATRA.png" alt="Maurice">'+
                                    "<figcaption>"+val.upto+"</figcaption>"+
                                '</figure> '+                                           
                            '</div>   '+                                         
                            '<div class="carousel-item">'+
                                '<figure>'+
                                    '<img class="d-block w-100" src="assets/images/NOKU_MALDIVES.png" alt="Maurice">'+
                                    "<figcaption>"+val.upto+"</figcaption>"+
                                '</figure>'+
                            '</div>'+
                        '</div>'+

                        '<a class="carousel-control-prev" href="#carouselExampleControls5" role="button" data-slide="prev">'+
                            '<span class="carousel-control-prev-icon" aria-hidden="true"></span>'+
                            '<span class="sr-only">Previous</span>'+
                        '</a>'+
                        '<a class="carousel-control-next" href="#carouselExampleControls5" role="button" data-slide="next">'+
                            '<span class="carousel-control-next-icon" aria-hidden="true"></span>'+
                            '<span class="sr-only">Next</span>'+
                        '</a>'+
                    '</div>     '+                           
                    '<div class="card-body">'+
                        '<div>'+
                            '<h5 class="card-title">'+val.country+' <span class="country">'+val.place+'</span></h5>'+
                            '<p class="card-text"> '+
                                '<i class="fas fa-star" style="font-size: 14px;"></i>'+
                                '<i class="fas fa-star" style="font-size: 14px;"></i>'+
                                '<i class="fas fa-star" style="font-size: 14px;"></i>'+
                                '<i class="fas fa-star" style="font-size: 14px;"></i>'+
                                '<i class="fas fa-star" style="font-size: 14px;"></i>'+
                            '</p>'+
                            '<label class="premium">'+val.tags[0].label+'</label>'+
                            '<label class="option">'+val.tag[1].label+'</label>'+
                        '</div>'+
                        '<div>'+
                            '<a href="#" class="btn link"> <i class="carousel-control-next-icon" aria-hidden="true"></i></a>     '+                                  
                        '</div>             '+                       
                    '</div>'+
                '</div>'+
            '</div>'
            );
        });
        
        //$("#all-destiantion .row").append(items);
    });                
});